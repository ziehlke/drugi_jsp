<%@ page import="java.util.Enumeration" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.Date" %>
<%@ page import="static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME" %>
<%@ page import="java.sql.Timestamp" %>
<%--
  Created by IntelliJ IDEA.
  User: cielke
  Date: 02.12.18
  Time: 12:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello JSP Attributes</title>
</head>
<body>

<h1>JSP Attributes</h1>

<h2>HTTP readers:</h2>

<%--ZADANIE1--%>

<table>
    <thead>
    <tr>
        <td><b>name</b></td>
        <td><b>value</b></td>
    </tr>
    </thead>
    <tbody>
    <%
        Enumeration<String> headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            String row = String.format("<tr><td>%s</td><td>%s</td></tr>", header, request.getHeader(header));
            out.print(row);
        }
    %>
    </tbody>
</table>


<%--ZADANIE2--%>

<h2>like cookies? catch:</h2>

<table>
    <thead>
    <tr>
        <td><b>name</b></td>
        <td><b>value</b></td>
    </tr>
    </thead>
    <tbody>
    <%
        Cookie[] cookies = request.getCookies();

        for (Cookie cookie : cookies) {
            out.println(String.format("<tr><td>%s</td><td>%s</td></tr>", cookie.getName(), cookie.getValue()));
        }
    %>
    </tbody>
</table>


<%--ZADANIE3--%>

<h2>want more cookies? add some:</h2>


<form action="index.jsp" method="POST">
    Cookie name: <input type="text" name="cookieName">
    <br/>
    Cookie value: <input type="text" name="cookieValue"/>
    <br/>
    Cookie expiry date: <input type="datetime-local" name="cookieExpiryDate"/>
    <input type="submit" value="Submit"/>
</form>


<%
    String cookieName = request.getParameter("cookieName");
    String cookieValue = request.getParameter("cookieValue");
    String expieryDate = request.getParameter("cookieExpiryDate");

    if (cookieName != null && !cookieName.isEmpty() && cookieValue != null && !cookieValue.isEmpty()) {
        Cookie cookie = new Cookie(cookieName, cookieValue);

        LocalDateTime parsedDate = LocalDateTime.parse(expieryDate, ISO_LOCAL_DATE_TIME);
        long timeStamp = Timestamp.valueOf(parsedDate).getTime();
        long now = Timestamp.valueOf(LocalDateTime.now()).getTime();

        Integer secondsToLive = Integer.valueOf(String.valueOf((timeStamp - now) / 1_000));
        cookie.setMaxAge(secondsToLive);
        response.addCookie(cookie);

        out.print("your cookie will live for another: " + secondsToLive / 60 + "minutes.");
    }
%>


<%--ZADANIE4--%>

<h2>Session attributes</h2>

<table>
    <thead>
    <tr>
        <td><b>key</b></td>
        <td><b>value</b></td>
    </tr>
    </thead>
    <tbody>

    <%
        Enumeration<String> attributeNames = session.getAttributeNames();

        while (attributeNames.hasMoreElements()) {
            String element = attributeNames.nextElement();
            out.println(String.format("<tr><td>%s</td><td>%s</td></tr>", element, (String)session.getAttribute(element)));
        }
    %>
    </tbody>
</table>


<%--ZADANIE5--%>
<form action="index.jsp" method="POST">
    Session key: <input type="text" name="sessionKey">
    <br/>
    Session key value: <input type="text" name="sessionValue"/>
    <input type="submit" value="Submit"/>
</form>


<%
    String sessionKey = request.getParameter("sessionKey");
    String sessionValue = request.getParameter("sessionValue");

    if (sessionKey != null && !sessionKey.isEmpty() && sessionValue != null && !sessionValue.isEmpty()) {
        session.setAttribute(sessionKey, sessionValue);
    }
%>



</body>
</html>
